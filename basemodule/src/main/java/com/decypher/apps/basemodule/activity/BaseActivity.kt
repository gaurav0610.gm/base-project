package com.decypher.apps.basemodule.activity

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.decypher.apps.basemodule.R
import com.decypher.apps.basemodule.databinding.ActivityBaseBinding

abstract class BaseActivity<T: ViewDataBinding> :AppCompatActivity() {

    lateinit var mActivityBaseBinding: ActivityBaseBinding
    lateinit var mContainerBinding: T

    @LayoutRes
    protected abstract fun getLayoutResourceId(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mActivityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base)
        mContainerBinding = DataBindingUtil.inflate(inflater, getLayoutResourceId(), (mActivityBaseBinding.baseActivityFrame), true)

    }

    fun getBaseActivity():ActivityBaseBinding? {
        return mActivityBaseBinding
    }

    fun getContainerActivity(): T {
        return mContainerBinding
    }

    fun showOverlayProgressBar(){
        mActivityBaseBinding.progressBar.visibility = View.VISIBLE
        mActivityBaseBinding.baseLlOverlay.visibility = View.VISIBLE
    }

    fun showProgressBarOnly(){
        mActivityBaseBinding.progressBar.visibility = View.VISIBLE
    }

    fun hideOverlayProgressBar(){
        mActivityBaseBinding.progressBar.visibility = View.GONE
        mActivityBaseBinding.baseLlOverlay.visibility = View.GONE
    }

    fun hideProgressBarOnly(){
        mActivityBaseBinding.progressBar.visibility = View.GONE
    }

}